"""
Licensed under the MIT License.

Copyright (c) 2021 - present Alex Rhodes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import random
from Crypto.Cipher import AES
from Crypto.Util import Counter

#Key
key =    0x97247d91d32fa1f6bece5da9bfe61c1a3b32edf26fd6ec2a6187ba777fc3c1d8

#IV
iv =   0x37b30c3bd7618415fbb9c7f400000000

#Input data
plain_text = [
    0xAAAAAAAA, 0xBBBBBBBB, 0xCCCCCCCC, 0xDDDDDDDD,
    0xEEEEEEEE, 0xFFFFFFFF, 0x11111111, 0x22222222,
    0x33333333, 0x44444444, 0x55555555, 0x66666666,
    0x77777777, 0x88888888, 0x99999999, 0xF1F1F1F1
]

#Create a byte array of the input words
plaintext = bytearray()
for p in plain_text:
    plaintext.extend(p.to_bytes(4,'big'))

#Counter
ctr = Counter.new(128, initial_value=iv)

#AES 256 CTR
aesCTR = AES.new(key=key.to_bytes(32,'big'), mode=AES.MODE_CTR, counter=ctr)

#Encrypt
cipher = aesCTR.encrypt(bytes(plaintext))

#Convert to a number
cipher = int.from_bytes(cipher,'big')

#Display the result
outs = hex(cipher).upper()[2:]
outs = ' '.join(outs[i:i+8] for i in range(0, len(outs), 8))
print("Cipher text:\n{}".format(outs))