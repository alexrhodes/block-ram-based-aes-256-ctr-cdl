/*
Licensed under the MIT License.

Copyright (c) 2021 - present Alex Rhodes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "xil_printf.h"
#include "unistd.h"
#include "stdint.h"
#include "registers.h"
#include "aes.h"

#define TEST_WORD_COUNT 16

///Arbitrary test input
uint32_t plain_text[TEST_WORD_COUNT] =
{
    0xAAAAAAAA, 0xBBBBBBBB, 0xCCCCCCCC, 0xDDDDDDDD,
    0xEEEEEEEE, 0xFFFFFFFF, 0x11111111, 0x22222222,
    0x33333333, 0x44444444, 0x55555555, 0x66666666,
    0x77777777, 0x88888888, 0x99999999, 0xF1F1F1F1
};

///A random 256-bit AES key
uint32_t key[AES_256_KEY_WORDS] =
{
    0x97247d91, 0xd32fa1f6, 0xbece5da9, 0xbfe61c1a,
    0x3b32edf2, 0x6fd6ec2a, 0x6187ba77, 0x7fc3c1d8
};

///A random initialization vector
uint32_t iv[AES_256_IV_WORDS] =
{
    0x37b30c3b, 0xd7618415, 0xfbb9c7f4, 0x00000000
};

///Destination buffer for encrypted input
uint32_t cipher_text[TEST_WORD_COUNT];

//Destination buffer for round-trip decryption
uint32_t round_trip_plain_text[TEST_WORD_COUNT];


//Expected cipher text for error checking
uint32_t exp_cipher_text[TEST_WORD_COUNT] =
{
    0x36903C3F, 0xFACAEEDC, 0x7795B402, 0xF9521F52,
    0x8EEBB50A, 0x9E9FD113, 0x4A0D0126, 0xE7473069,
    0xDFB0AD85, 0x49A08116, 0xE1F86B4E, 0xE290AF93,
    0x2BCC909C, 0x89F7F05B, 0xB336384B, 0x1BA18132
};

int main()
{

    xil_printf("AES test starting.\n\r");
    xil_printf("Version: %x \n\r", reg_read(VERSION_REG));

    xil_printf("Encrypting... \n\r");
    aes256Ctr_encrypt(cipher_text, plain_text, TEST_WORD_COUNT, key, iv);

    uint32_t enc_errors = 0;
    xil_printf("Encrypted data: \n\r");
    for(int i = 0; i < TEST_WORD_COUNT; i++)
    {
        xil_printf("%08x ", cipher_text[i]);
        if(cipher_text[i] != exp_cipher_text[i])
            enc_errors++;
    }
    xil_printf("\n\r");
    xil_printf("Encryption errors: %d \n\r", enc_errors);
    xil_printf("Decrypting... \n\r");
    aes256Ctr_decrypt(round_trip_plain_text, cipher_text, TEST_WORD_COUNT, key, iv);

    uint32_t dec_errors = 0;
    xil_printf("Round-trip data: \n\r");
    for(int i = 0; i < TEST_WORD_COUNT; i++)
    {
        xil_printf("%08x ", round_trip_plain_text[i]);
        if(round_trip_plain_text[i] != plain_text[i])
            dec_errors++;
    }
    xil_printf("\n\r");
    xil_printf("Round-trip errors: %d \n\r", dec_errors);

    if(enc_errors == 0 && dec_errors == 0)
    {
        xil_printf("Test passed.\n\r");
    }
    else
    {
        xil_printf("Test failed, encryption errors: %d decryption errors: %d",
                   enc_errors, dec_errors);
    }
    xil_printf("Test complete. \n\r");

}