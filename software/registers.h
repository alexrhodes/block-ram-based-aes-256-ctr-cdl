/*
Licensed under the MIT License.

Copyright (c) 2021 - present Alex Rhodes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef REGISTERS_H
#define REGISTERS_H
#include <stdint.h>

///This enum is the register memory mapping
typedef enum
{
    AES_CTRL_BASE      = 0x43c00000,
    VERSION_REG        = (AES_CTRL_BASE),
    STATUS_CTRL_REG    = (AES_CTRL_BASE + 4),
    KEY_0              = (AES_CTRL_BASE + 8),
    KEY_1              = (AES_CTRL_BASE + 12),
    KEY_2              = (AES_CTRL_BASE + 16),
    KEY_3              = (AES_CTRL_BASE + 20),
    KEY_4              = (AES_CTRL_BASE + 24),
    KEY_5              = (AES_CTRL_BASE + 28),
    KEY_6              = (AES_CTRL_BASE + 32),
    KEY_7              = (AES_CTRL_BASE + 36),
    IV_0               = (AES_CTRL_BASE + 40),
    IV_1               = (AES_CTRL_BASE + 44),
    IV_2               = (AES_CTRL_BASE + 48),
    IV_3               = (AES_CTRL_BASE + 52),
    NUM_BLOCKS         = (AES_CTRL_BASE + 56),
    BASE_ADDR          = (AES_CTRL_BASE + 60)
}REGISTER;

///This function is for reading registers
extern uint32_t reg_read(REGISTER reg);

///This function is for writing registers
extern void reg_write(REGISTER reg, uint32_t val);

#endif
