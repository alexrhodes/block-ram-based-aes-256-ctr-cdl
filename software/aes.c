/*
Licensed under the MIT License.

Copyright (c) 2021 - present Alex Rhodes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "aes.h"
#include "assert.h"
#include "registers.h"
#include "unistd.h"
#include "xil_printf.h"

#define AES_MEM_BASE  0x40000000 //!< Base address of the AES BRAM
#define RELEASE 0b10             //!< Release AES CTR from reset
#define ENABLE  0b11             //!< Enable AES CTR module
#define DISABLE 0                //!< Disabel AES CTR module
#define DONE_BIT 0b100           //!< Done bit position



void aes256Ctr_encrypt(uint32_t * dest,
                       uint32_t * src,
                       uint32_t num_words,
                       uint32_t * key,
                       uint32_t * iv)
{
    //Take AES module out of reset
    reg_write(STATUS_CTRL_REG, RELEASE);

    usleep(2000);

    //Set up the key
    reg_write(KEY_0, key[0]);
    reg_write(KEY_1, key[1]);
    reg_write(KEY_2, key[2]);
    reg_write(KEY_3, key[3]);
    reg_write(KEY_4, key[4]);
    reg_write(KEY_5, key[5]);
    reg_write(KEY_6, key[6]);
    reg_write(KEY_7, key[7]);

    //Set up the IV
    reg_write(IV_0, iv[0]);
    reg_write(IV_1, iv[1]);
    reg_write(IV_2, iv[2]);
    reg_write(IV_3, iv[3]);

    //Set the number of blocks, assert correct padding/alignment
    assert(num_words % 4 == 0);
    assert(num_words != 0);
    reg_write(NUM_BLOCKS, num_words / 4);

    //Copy the data into the AES BRAM, setting the word ordering MSW - LSW
    uint32_t * dataPtr = (uint32_t*)AES_MEM_BASE;
    for(int i = 0; i < num_words; i+=4)
    {
        dataPtr[i+3] = src[i];
        dataPtr[i+2] = src[i+1];
        dataPtr[i+1] = src[i+2];
        dataPtr[i]   = src[i+3];
    }

    //Set the base address
    reg_write(BASE_ADDR, AES_MEM_BASE);

    //Enable
    reg_write(STATUS_CTRL_REG, ENABLE);
    usleep(5000);

    //Poll for done or timeout
    uint32_t timeoutCount = 0;
    uint32_t done = reg_read(STATUS_CTRL_REG) & DONE_BIT;
    while(done == 0 && timeoutCount < 10000)
    {
        done = reg_read(STATUS_CTRL_REG) & DONE_BIT;
        timeoutCount++;
    }
    if(timeoutCount == 10000)
    {
        xil_printf("Timed out waiting for done! \n\r");
    }
    assert(timeoutCount != 10000);

    //Copy the output into the destination buffer, reverting word ordering
    for(int i = 0; i < num_words; i+=4)
    {
        dest[i]   = dataPtr[i+3];
        dest[i+1] = dataPtr[i+2];
        dest[i+2] = dataPtr[i+1];
        dest[i+3] = dataPtr[i];
    }

    //Disable
    reg_write(STATUS_CTRL_REG, DISABLE);
}


void aes256Ctr_decrypt(uint32_t * dest,
                       uint32_t * src,
                       uint32_t num_words,
                       uint32_t * key,
                       uint32_t * iv)
{
    //Encryption and decryption are the same operation
    aes256Ctr_encrypt(dest, src, num_words, key, iv);
}

