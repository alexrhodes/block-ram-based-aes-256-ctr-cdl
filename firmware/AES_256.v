/*
Licensed under the MIT License.

Copyright (c) 2021 - present Alex Rhodes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
`timescale 1ns / 1ps
//Data Encryption Standard IP block
module AES256
    (
        input clock,                 //Clock signal
        input enable,                //Enable signal
        input resetn,                //Reset signal
        input [255:0] key,           //Input key
        input [127:0] data_in,       //Input data
        output reg [127:0] data_out, //Output data
        output wire valid            //Valid signal
    );

reg enable_ff1; //Enable pulse flipflops
reg enable_ff2;
reg [3:0] state_reg; ///This register holds the current state


reg [7:0] s_box [0:15][0:15];    //Substition boxes
reg [7:0] inv_s_box[0:15][0:15]; //Inverse Substition boxes
 
reg [7:0] r_con [0:9];        //Round constants
reg [127:0] round_keys[0:15]; //Round key storage
reg [31:0] key_words[0:59];   //Key word storage

//This list of state matrices forms the data pipeline
reg [7:0] initial_matrix[0:3][0:3];   //Initial state matrix
reg [7:0] round_key_matrix[0:3][0:3]; //Round key matrix
reg [7:0] sub_byte_matrix[0:3][0:3];  //Byte substitution matrix
reg [7:0] shift_row_matrix[0:3][0:3]; //Shift rows matrix
reg [7:0] mix_col_matrix[0:3][0:3];   //Mix columns matrix

//Global round counter
reg [3:0] round_counter;
initial begin
    round_counter = 0;
end

///State machine states
parameter [4:0] IDLE      =   4'b0000, //State machine is idle
                KEY_INIT  =   4'b0001, //Initial key words set
                KEY_WORDS =   4'b0010, //Key schedule
                INIT_MATRIX = 4'b0011, //Initial data matrix set
                ADD_R0_KEY =  4'b0100, //Add first round key
                ADD_KEY    =  4'b0101, //Add round key layer
                SUB_BYTE   =  4'b0110, //Byte substitution layer
                SHIFT_ROW  =  4'b0111, //Shift rows layer
                MIX_COL    =  4'b1000, //Mix columns layer
                OUTPUT     =  4'b1001, //Output data is set
                DONE       =  4'b1010; //Operation complete

initial begin
//Initialize the S-box
s_box[0][0] = 8'h63;
s_box[0][1] = 8'h7c;
s_box[0][2] = 8'h77;
s_box[0][3] = 8'h7b;
s_box[0][4] = 8'hf2;
s_box[0][5] = 8'h6b;
s_box[0][6] = 8'h6f;
s_box[0][7] = 8'hc5;
s_box[0][8] = 8'h30;
s_box[0][9] = 8'h1;
s_box[0][10] = 8'h67;
s_box[0][11] = 8'h2b;
s_box[0][12] = 8'hfe;
s_box[0][13] = 8'hd7;
s_box[0][14] = 8'hab;
s_box[0][15] = 8'h76;
s_box[1][0] = 8'hca;
s_box[1][1] = 8'h82;
s_box[1][2] = 8'hc9;
s_box[1][3] = 8'h7d;
s_box[1][4] = 8'hfa;
s_box[1][5] = 8'h59;
s_box[1][6] = 8'h47;
s_box[1][7] = 8'hf0;
s_box[1][8] = 8'had;
s_box[1][9] = 8'hd4;
s_box[1][10] = 8'ha2;
s_box[1][11] = 8'haf;
s_box[1][12] = 8'h9c;
s_box[1][13] = 8'ha4;
s_box[1][14] = 8'h72;
s_box[1][15] = 8'hc0;
s_box[2][0] = 8'hb7;
s_box[2][1] = 8'hfd;
s_box[2][2] = 8'h93;
s_box[2][3] = 8'h26;
s_box[2][4] = 8'h36;
s_box[2][5] = 8'h3f;
s_box[2][6] = 8'hf7;
s_box[2][7] = 8'hcc;
s_box[2][8] = 8'h34;
s_box[2][9] = 8'ha5;
s_box[2][10] = 8'he5;
s_box[2][11] = 8'hf1;
s_box[2][12] = 8'h71;
s_box[2][13] = 8'hd8;
s_box[2][14] = 8'h31;
s_box[2][15] = 8'h15;
s_box[3][0] = 8'h4;
s_box[3][1] = 8'hc7;
s_box[3][2] = 8'h23;
s_box[3][3] = 8'hc3;
s_box[3][4] = 8'h18;
s_box[3][5] = 8'h96;
s_box[3][6] = 8'h5;
s_box[3][7] = 8'h9a;
s_box[3][8] = 8'h7;
s_box[3][9] = 8'h12;
s_box[3][10] = 8'h80;
s_box[3][11] = 8'he2;
s_box[3][12] = 8'heb;
s_box[3][13] = 8'h27;
s_box[3][14] = 8'hb2;
s_box[3][15] = 8'h75;
s_box[4][0] = 8'h9;
s_box[4][1] = 8'h83;
s_box[4][2] = 8'h2c;
s_box[4][3] = 8'h1a;
s_box[4][4] = 8'h1b;
s_box[4][5] = 8'h6e;
s_box[4][6] = 8'h5a;
s_box[4][7] = 8'ha0;
s_box[4][8] = 8'h52;
s_box[4][9] = 8'h3b;
s_box[4][10] = 8'hd6;
s_box[4][11] = 8'hb3;
s_box[4][12] = 8'h29;
s_box[4][13] = 8'he3;
s_box[4][14] = 8'h2f;
s_box[4][15] = 8'h84;
s_box[5][0] = 8'h53;
s_box[5][1] = 8'hd1;
s_box[5][2] = 8'h0;
s_box[5][3] = 8'hed;
s_box[5][4] = 8'h20;
s_box[5][5] = 8'hfc;
s_box[5][6] = 8'hb1;
s_box[5][7] = 8'h5b;
s_box[5][8] = 8'h6a;
s_box[5][9] = 8'hcb;
s_box[5][10] = 8'hbe;
s_box[5][11] = 8'h39;
s_box[5][12] = 8'h4a;
s_box[5][13] = 8'h4c;
s_box[5][14] = 8'h58;
s_box[5][15] = 8'hcf;
s_box[6][0] = 8'hd0;
s_box[6][1] = 8'hef;
s_box[6][2] = 8'haa;
s_box[6][3] = 8'hfb;
s_box[6][4] = 8'h43;
s_box[6][5] = 8'h4d;
s_box[6][6] = 8'h33;
s_box[6][7] = 8'h85;
s_box[6][8] = 8'h45;
s_box[6][9] = 8'hf9;
s_box[6][10] = 8'h2;
s_box[6][11] = 8'h7f;
s_box[6][12] = 8'h50;
s_box[6][13] = 8'h3c;
s_box[6][14] = 8'h9f;
s_box[6][15] = 8'ha8;
s_box[7][0] = 8'h51;
s_box[7][1] = 8'ha3;
s_box[7][2] = 8'h40;
s_box[7][3] = 8'h8f;
s_box[7][4] = 8'h92;
s_box[7][5] = 8'h9d;
s_box[7][6] = 8'h38;
s_box[7][7] = 8'hf5;
s_box[7][8] = 8'hbc;
s_box[7][9] = 8'hb6;
s_box[7][10] = 8'hda;
s_box[7][11] = 8'h21;
s_box[7][12] = 8'h10;
s_box[7][13] = 8'hff;
s_box[7][14] = 8'hf3;
s_box[7][15] = 8'hd2;
s_box[8][0] = 8'hcd;
s_box[8][1] = 8'hc;
s_box[8][2] = 8'h13;
s_box[8][3] = 8'hec;
s_box[8][4] = 8'h5f;
s_box[8][5] = 8'h97;
s_box[8][6] = 8'h44;
s_box[8][7] = 8'h17;
s_box[8][8] = 8'hc4;
s_box[8][9] = 8'ha7;
s_box[8][10] = 8'h7e;
s_box[8][11] = 8'h3d;
s_box[8][12] = 8'h64;
s_box[8][13] = 8'h5d;
s_box[8][14] = 8'h19;
s_box[8][15] = 8'h73;
s_box[9][0] = 8'h60;
s_box[9][1] = 8'h81;
s_box[9][2] = 8'h4f;
s_box[9][3] = 8'hdc;
s_box[9][4] = 8'h22;
s_box[9][5] = 8'h2a;
s_box[9][6] = 8'h90;
s_box[9][7] = 8'h88;
s_box[9][8] = 8'h46;
s_box[9][9] = 8'hee;
s_box[9][10] = 8'hb8;
s_box[9][11] = 8'h14;
s_box[9][12] = 8'hde;
s_box[9][13] = 8'h5e;
s_box[9][14] = 8'hb;
s_box[9][15] = 8'hdb;
s_box[10][0] = 8'he0;
s_box[10][1] = 8'h32;
s_box[10][2] = 8'h3a;
s_box[10][3] = 8'ha;
s_box[10][4] = 8'h49;
s_box[10][5] = 8'h6;
s_box[10][6] = 8'h24;
s_box[10][7] = 8'h5c;
s_box[10][8] = 8'hc2;
s_box[10][9] = 8'hd3;
s_box[10][10] = 8'hac;
s_box[10][11] = 8'h62;
s_box[10][12] = 8'h91;
s_box[10][13] = 8'h95;
s_box[10][14] = 8'he4;
s_box[10][15] = 8'h79;
s_box[11][0] = 8'he7;
s_box[11][1] = 8'hc8;
s_box[11][2] = 8'h37;
s_box[11][3] = 8'h6d;
s_box[11][4] = 8'h8d;
s_box[11][5] = 8'hd5;
s_box[11][6] = 8'h4e;
s_box[11][7] = 8'ha9;
s_box[11][8] = 8'h6c;
s_box[11][9] = 8'h56;
s_box[11][10] = 8'hf4;
s_box[11][11] = 8'hea;
s_box[11][12] = 8'h65;
s_box[11][13] = 8'h7a;
s_box[11][14] = 8'hae;
s_box[11][15] = 8'h8;
s_box[12][0] = 8'hba;
s_box[12][1] = 8'h78;
s_box[12][2] = 8'h25;
s_box[12][3] = 8'h2e;
s_box[12][4] = 8'h1c;
s_box[12][5] = 8'ha6;
s_box[12][6] = 8'hb4;
s_box[12][7] = 8'hc6;
s_box[12][8] = 8'he8;
s_box[12][9] = 8'hdd;
s_box[12][10] = 8'h74;
s_box[12][11] = 8'h1f;
s_box[12][12] = 8'h4b;
s_box[12][13] = 8'hbd;
s_box[12][14] = 8'h8b;
s_box[12][15] = 8'h8a;
s_box[13][0] = 8'h70;
s_box[13][1] = 8'h3e;
s_box[13][2] = 8'hb5;
s_box[13][3] = 8'h66;
s_box[13][4] = 8'h48;
s_box[13][5] = 8'h3;
s_box[13][6] = 8'hf6;
s_box[13][7] = 8'he;
s_box[13][8] = 8'h61;
s_box[13][9] = 8'h35;
s_box[13][10] = 8'h57;
s_box[13][11] = 8'hb9;
s_box[13][12] = 8'h86;
s_box[13][13] = 8'hc1;
s_box[13][14] = 8'h1d;
s_box[13][15] = 8'h9e;
s_box[14][0] = 8'he1;
s_box[14][1] = 8'hf8;
s_box[14][2] = 8'h98;
s_box[14][3] = 8'h11;
s_box[14][4] = 8'h69;
s_box[14][5] = 8'hd9;
s_box[14][6] = 8'h8e;
s_box[14][7] = 8'h94;
s_box[14][8] = 8'h9b;
s_box[14][9] = 8'h1e;
s_box[14][10] = 8'h87;
s_box[14][11] = 8'he9;
s_box[14][12] = 8'hce;
s_box[14][13] = 8'h55;
s_box[14][14] = 8'h28;
s_box[14][15] = 8'hdf;
s_box[15][0] = 8'h8c;
s_box[15][1] = 8'ha1;
s_box[15][2] = 8'h89;
s_box[15][3] = 8'hd;
s_box[15][4] = 8'hbf;
s_box[15][5] = 8'he6;
s_box[15][6] = 8'h42;
s_box[15][7] = 8'h68;
s_box[15][8] = 8'h41;
s_box[15][9] = 8'h99;
s_box[15][10] = 8'h2d;
s_box[15][11] = 8'hf;
s_box[15][12] = 8'hb0;
s_box[15][13] = 8'h54;
s_box[15][14] = 8'hbb;
s_box[15][15] = 8'h16;

//Initialize the inverse s-box
inv_s_box[0][0] = 8'h52;
inv_s_box[0][1] = 8'h9;
inv_s_box[0][2] = 8'h6a;
inv_s_box[0][3] = 8'hd5;
inv_s_box[0][4] = 8'h30;
inv_s_box[0][5] = 8'h36;
inv_s_box[0][6] = 8'ha5;
inv_s_box[0][7] = 8'h38;
inv_s_box[0][8] = 8'hbf;
inv_s_box[0][9] = 8'h40;
inv_s_box[0][10] = 8'ha3;
inv_s_box[0][11] = 8'h9e;
inv_s_box[0][12] = 8'h81;
inv_s_box[0][13] = 8'hf3;
inv_s_box[0][14] = 8'hd7;
inv_s_box[0][15] = 8'hfb;
inv_s_box[1][0] = 8'h7c;
inv_s_box[1][1] = 8'he3;
inv_s_box[1][2] = 8'h39;
inv_s_box[1][3] = 8'h82;
inv_s_box[1][4] = 8'h9b;
inv_s_box[1][5] = 8'h2f;
inv_s_box[1][6] = 8'hff;
inv_s_box[1][7] = 8'h87;
inv_s_box[1][8] = 8'h34;
inv_s_box[1][9] = 8'h8e;
inv_s_box[1][10] = 8'h43;
inv_s_box[1][11] = 8'h44;
inv_s_box[1][12] = 8'hc4;
inv_s_box[1][13] = 8'hde;
inv_s_box[1][14] = 8'he9;
inv_s_box[1][15] = 8'hcb;
inv_s_box[2][0] = 8'h54;
inv_s_box[2][1] = 8'h7b;
inv_s_box[2][2] = 8'h94;
inv_s_box[2][3] = 8'h32;
inv_s_box[2][4] = 8'ha6;
inv_s_box[2][5] = 8'hc2;
inv_s_box[2][6] = 8'h23;
inv_s_box[2][7] = 8'h3d;
inv_s_box[2][8] = 8'hee;
inv_s_box[2][9] = 8'h4c;
inv_s_box[2][10] = 8'h95;
inv_s_box[2][11] = 8'hb;
inv_s_box[2][12] = 8'h42;
inv_s_box[2][13] = 8'hfa;
inv_s_box[2][14] = 8'hc3;
inv_s_box[2][15] = 8'h4e;
inv_s_box[3][0] = 8'h8;
inv_s_box[3][1] = 8'h2e;
inv_s_box[3][2] = 8'ha1;
inv_s_box[3][3] = 8'h66;
inv_s_box[3][4] = 8'h28;
inv_s_box[3][5] = 8'hd9;
inv_s_box[3][6] = 8'h24;
inv_s_box[3][7] = 8'hb2;
inv_s_box[3][8] = 8'h76;
inv_s_box[3][9] = 8'h5b;
inv_s_box[3][10] = 8'ha2;
inv_s_box[3][11] = 8'h49;
inv_s_box[3][12] = 8'h6d;
inv_s_box[3][13] = 8'h8b;
inv_s_box[3][14] = 8'hd1;
inv_s_box[3][15] = 8'h25;
inv_s_box[4][0] = 8'h72;
inv_s_box[4][1] = 8'hf8;
inv_s_box[4][2] = 8'hf6;
inv_s_box[4][3] = 8'h64;
inv_s_box[4][4] = 8'h86;
inv_s_box[4][5] = 8'h68;
inv_s_box[4][6] = 8'h98;
inv_s_box[4][7] = 8'h16;
inv_s_box[4][8] = 8'hd4;
inv_s_box[4][9] = 8'ha4;
inv_s_box[4][10] = 8'h5c;
inv_s_box[4][11] = 8'hcc;
inv_s_box[4][12] = 8'h5d;
inv_s_box[4][13] = 8'h65;
inv_s_box[4][14] = 8'hb6;
inv_s_box[4][15] = 8'h92;
inv_s_box[5][0] = 8'h6c;
inv_s_box[5][1] = 8'h70;
inv_s_box[5][2] = 8'h48;
inv_s_box[5][3] = 8'h50;
inv_s_box[5][4] = 8'hfd;
inv_s_box[5][5] = 8'hed;
inv_s_box[5][6] = 8'hb9;
inv_s_box[5][7] = 8'hda;
inv_s_box[5][8] = 8'h5e;
inv_s_box[5][9] = 8'h15;
inv_s_box[5][10] = 8'h46;
inv_s_box[5][11] = 8'h57;
inv_s_box[5][12] = 8'ha7;
inv_s_box[5][13] = 8'h8d;
inv_s_box[5][14] = 8'h9d;
inv_s_box[5][15] = 8'h84;
inv_s_box[6][0] = 8'h90;
inv_s_box[6][1] = 8'hd8;
inv_s_box[6][2] = 8'hab;
inv_s_box[6][3] = 8'h0;
inv_s_box[6][4] = 8'h8c;
inv_s_box[6][5] = 8'hbc;
inv_s_box[6][6] = 8'hd3;
inv_s_box[6][7] = 8'ha;
inv_s_box[6][8] = 8'hf7;
inv_s_box[6][9] = 8'he4;
inv_s_box[6][10] = 8'h58;
inv_s_box[6][11] = 8'h5;
inv_s_box[6][12] = 8'hb8;
inv_s_box[6][13] = 8'hb3;
inv_s_box[6][14] = 8'h45;
inv_s_box[6][15] = 8'h6;
inv_s_box[7][0] = 8'hd0;
inv_s_box[7][1] = 8'h2c;
inv_s_box[7][2] = 8'h1e;
inv_s_box[7][3] = 8'h8f;
inv_s_box[7][4] = 8'hca;
inv_s_box[7][5] = 8'h3f;
inv_s_box[7][6] = 8'hf;
inv_s_box[7][7] = 8'h2;
inv_s_box[7][8] = 8'hc1;
inv_s_box[7][9] = 8'haf;
inv_s_box[7][10] = 8'hbd;
inv_s_box[7][11] = 8'h3;
inv_s_box[7][12] = 8'h1;
inv_s_box[7][13] = 8'h13;
inv_s_box[7][14] = 8'h8a;
inv_s_box[7][15] = 8'h6b;
inv_s_box[8][0] = 8'h3a;
inv_s_box[8][1] = 8'h91;
inv_s_box[8][2] = 8'h11;
inv_s_box[8][3] = 8'h41;
inv_s_box[8][4] = 8'h4f;
inv_s_box[8][5] = 8'h67;
inv_s_box[8][6] = 8'hdc;
inv_s_box[8][7] = 8'hea;
inv_s_box[8][8] = 8'h97;
inv_s_box[8][9] = 8'hf2;
inv_s_box[8][10] = 8'hcf;
inv_s_box[8][11] = 8'hce;
inv_s_box[8][12] = 8'hf0;
inv_s_box[8][13] = 8'hb4;
inv_s_box[8][14] = 8'he6;
inv_s_box[8][15] = 8'h73;
inv_s_box[9][0] = 8'h96;
inv_s_box[9][1] = 8'hac;
inv_s_box[9][2] = 8'h74;
inv_s_box[9][3] = 8'h22;
inv_s_box[9][4] = 8'he7;
inv_s_box[9][5] = 8'had;
inv_s_box[9][6] = 8'h35;
inv_s_box[9][7] = 8'h85;
inv_s_box[9][8] = 8'he2;
inv_s_box[9][9] = 8'hf9;
inv_s_box[9][10] = 8'h37;
inv_s_box[9][11] = 8'he8;
inv_s_box[9][12] = 8'h1c;
inv_s_box[9][13] = 8'h75;
inv_s_box[9][14] = 8'hdf;
inv_s_box[9][15] = 8'h6e;
inv_s_box[10][0] = 8'h47;
inv_s_box[10][1] = 8'hf1;
inv_s_box[10][2] = 8'h1a;
inv_s_box[10][3] = 8'h71;
inv_s_box[10][4] = 8'h1d;
inv_s_box[10][5] = 8'h29;
inv_s_box[10][6] = 8'hc5;
inv_s_box[10][7] = 8'h89;
inv_s_box[10][8] = 8'h6f;
inv_s_box[10][9] = 8'hb7;
inv_s_box[10][10] = 8'h62;
inv_s_box[10][11] = 8'he;
inv_s_box[10][12] = 8'haa;
inv_s_box[10][13] = 8'h18;
inv_s_box[10][14] = 8'hbe;
inv_s_box[10][15] = 8'h1b;
inv_s_box[11][0] = 8'hfc;
inv_s_box[11][1] = 8'h56;
inv_s_box[11][2] = 8'h3e;
inv_s_box[11][3] = 8'h4b;
inv_s_box[11][4] = 8'hc6;
inv_s_box[11][5] = 8'hd2;
inv_s_box[11][6] = 8'h79;
inv_s_box[11][7] = 8'h20;
inv_s_box[11][8] = 8'h9a;
inv_s_box[11][9] = 8'hdb;
inv_s_box[11][10] = 8'hc0;
inv_s_box[11][11] = 8'hfe;
inv_s_box[11][12] = 8'h78;
inv_s_box[11][13] = 8'hcd;
inv_s_box[11][14] = 8'h5a;
inv_s_box[11][15] = 8'hf4;
inv_s_box[12][0] = 8'h1f;
inv_s_box[12][1] = 8'hdd;
inv_s_box[12][2] = 8'ha8;
inv_s_box[12][3] = 8'h33;
inv_s_box[12][4] = 8'h88;
inv_s_box[12][5] = 8'h7;
inv_s_box[12][6] = 8'hc7;
inv_s_box[12][7] = 8'h31;
inv_s_box[12][8] = 8'hb1;
inv_s_box[12][9] = 8'h12;
inv_s_box[12][10] = 8'h10;
inv_s_box[12][11] = 8'h59;
inv_s_box[12][12] = 8'h27;
inv_s_box[12][13] = 8'h80;
inv_s_box[12][14] = 8'hec;
inv_s_box[12][15] = 8'h5f;
inv_s_box[13][0] = 8'h60;
inv_s_box[13][1] = 8'h51;
inv_s_box[13][2] = 8'h7f;
inv_s_box[13][3] = 8'ha9;
inv_s_box[13][4] = 8'h19;
inv_s_box[13][5] = 8'hb5;
inv_s_box[13][6] = 8'h4a;
inv_s_box[13][7] = 8'hd;
inv_s_box[13][8] = 8'h2d;
inv_s_box[13][9] = 8'he5;
inv_s_box[13][10] = 8'h7a;
inv_s_box[13][11] = 8'h9f;
inv_s_box[13][12] = 8'h93;
inv_s_box[13][13] = 8'hc9;
inv_s_box[13][14] = 8'h9c;
inv_s_box[13][15] = 8'hef;
inv_s_box[14][0] = 8'ha0;
inv_s_box[14][1] = 8'he0;
inv_s_box[14][2] = 8'h3b;
inv_s_box[14][3] = 8'h4d;
inv_s_box[14][4] = 8'hae;
inv_s_box[14][5] = 8'h2a;
inv_s_box[14][6] = 8'hf5;
inv_s_box[14][7] = 8'hb0;
inv_s_box[14][8] = 8'hc8;
inv_s_box[14][9] = 8'heb;
inv_s_box[14][10] = 8'hbb;
inv_s_box[14][11] = 8'h3c;
inv_s_box[14][12] = 8'h83;
inv_s_box[14][13] = 8'h53;
inv_s_box[14][14] = 8'h99;
inv_s_box[14][15] = 8'h61;
inv_s_box[15][0] = 8'h17;
inv_s_box[15][1] = 8'h2b;
inv_s_box[15][2] = 8'h4;
inv_s_box[15][3] = 8'h7e;
inv_s_box[15][4] = 8'hba;
inv_s_box[15][5] = 8'h77;
inv_s_box[15][6] = 8'hd6;
inv_s_box[15][7] = 8'h26;
inv_s_box[15][8] = 8'he1;
inv_s_box[15][9] = 8'h69;
inv_s_box[15][10] = 8'h14;
inv_s_box[15][11] = 8'h63;
inv_s_box[15][12] = 8'h55;
inv_s_box[15][13] = 8'h21;
inv_s_box[15][14] = 8'hc;
inv_s_box[15][15] = 8'h7d;

//Initialize the round constants
r_con[0] = 8'h1;
r_con[1] = 8'h2;
r_con[2] = 8'h4;
r_con[3] = 8'h8;
r_con[4] = 8'h10;
r_con[5] = 8'h20;
r_con[6] = 8'h40;
r_con[7] = 8'h80;
r_con[8] = 8'h1B;
r_con[9] = 8'h36;
end

//"Done" signal used to set the valid output
reg done;
assign valid = (done == 1'b1);

///This block generates an enable pulse
///to start the state machine when the
///enable signal is set high
initial begin
    done = 1'b0;
    enable_ff1 = 1'b0;
    enable_ff2 = 1'b0;
end

//Assign enable high for one clock cycle
assign enable_pulse    = (!enable_ff2) && enable_ff1;

always @(posedge clock) begin                                                        
    if (resetn == 1'b0 || enable == 1'b0)                                                   
        begin
        ///Reset                                                                 
            enable_ff1 <= 1'b0;                                                   
            enable_ff2 <= 1'b0;                                                   
        end                                                                               
    else                                                                       
        begin 
        /// Enable pulse
            enable_ff1 <= enable;
            enable_ff2 <= enable_ff1;                                         
        end                                                                      
    end   

//S-Box function
function [7:0] sbox;
    input [7:0] b;
    begin
        sbox = s_box[b[7:4]][b[3:0]];
    end
endfunction

//AES G function
function [31:0] g;
    input reg [31:0] word;
    input reg [7:0] rc;
    begin
        word     = l_rotate_word(word);
        g[31:24] = sbox(word[31:24]) ^ rc;
        g[23:16] = sbox(word[23:16]);
        g[15:8]  = sbox(word[15:8]);
        g[7:0]   = sbox(word[7:0]);
    end
endfunction

//AES H function
function [31:0] h;
    input reg [31:0] word;
    begin
        h[31:24] = sbox(word[31:24]);
        h[23:16] = sbox(word[23:16]);
        h[15:8]  = sbox(word[15:8]);
        h[7:0]   = sbox(word[7:0]);
    end
endfunction

//byte-wise left rotate of a word
function [31:0] l_rotate_word;
    input reg [31:0] word;
    begin
        l_rotate_word[31:8] = word[24:0];
        l_rotate_word[7:0]  = word[31:24]; 
    end
endfunction

///This function performs multiplication in 
///GF(2) using an adder in the AES finite field
//TODO: Loop removal would complicate the Mix Column
//      layer. For now leave it in.
function [7:0] gf2mult;
    input reg [7:0] x;
    input reg [7:0] y;
    integer i;
    reg [7:0] b;
    begin
        gf2mult = 0;
        for(i = 0; i < 8; i=i+1)
        begin
            if(y & 1'b1)
            begin
                gf2mult = gf2mult ^ x;
            end
            b = (x & 8'h80);
            x = (x << 1);
            if(b)
                x = x ^ 8'h1B;
            y = (y >> 1);
        end
    end
endfunction


///This state machine calculates the
///the round key words and round keys, setting its
///"done" register when finished
reg [5:0] table_idx;       //Key word index
reg [3:0] rcon_idx;        //Round constant index
reg [3:0] rk_idx;          //Round key index
parameter [4:0] KW_IDLE  = 4'b000, //Idle state
                KW_CALC  = 4'b001, //Calculate state
                KW_SCHED = 4'b010, //Form round keys
                KW_DONE  = 4'b011; //Done state
reg [2:0] kw_state;  //State machine states

//Done signal
wire KEY_WORDS_DONE;
assign KEY_WORDS_DONE = (kw_state == KW_DONE);
always @(posedge clock)begin
    if(resetn == 1'b0) begin
        //Reset
        table_idx <= 6'h8;
        rcon_idx  <= 4'b0;
        kw_state  <= 5'b0;
        rk_idx    <= 4'b0;
        //Reset round keys
        round_keys[0] <= 128'b0;
        round_keys[1] <= 128'b0;
        round_keys[2] <= 128'b0;
        round_keys[3] <= 128'b0;
        round_keys[4] <= 128'b0;
        round_keys[5] <= 128'b0;
        round_keys[6] <= 128'b0;
        round_keys[7] <= 128'b0;
        round_keys[8] <= 128'b0;
        round_keys[9] <= 128'b0;
        round_keys[10] <= 128'b0;
        round_keys[11] <= 128'b0;
        round_keys[12] <= 128'b0;
        round_keys[13] <= 128'b0;
        round_keys[14] <= 128'b0;
        round_keys[15] <= 128'b0;
        //Reset key words
        key_words[0] <= 32'b0;
        key_words[1] <= 32'b0;
        key_words[2] <= 32'b0;
        key_words[3] <= 32'b0;
        key_words[4] <= 32'b0;
        key_words[5] <= 32'b0;
        key_words[6] <= 32'b0;
        key_words[7] <= 32'b0;
        key_words[8] <= 32'b0;
        key_words[9] <= 32'b0;
        key_words[10] <= 32'b0;
        key_words[11] <= 32'b0;
        key_words[12] <= 32'b0;
        key_words[13] <= 32'b0;
        key_words[14] <= 32'b0;
        key_words[15] <= 32'b0;
        key_words[16] <= 32'b0;
        key_words[17] <= 32'b0;
        key_words[18] <= 32'b0;
        key_words[19] <= 32'b0;
        key_words[20] <= 32'b0;
        key_words[21] <= 32'b0;
        key_words[22] <= 32'b0;
        key_words[23] <= 32'b0;
        key_words[24] <= 32'b0;
        key_words[25] <= 32'b0;
        key_words[26] <= 32'b0;
        key_words[27] <= 32'b0;
        key_words[28] <= 32'b0;
        key_words[29] <= 32'b0;
        key_words[30] <= 32'b0;
        key_words[31] <= 32'b0;
        key_words[32] <= 32'b0;
        key_words[33] <= 32'b0;
        key_words[34] <= 32'b0;
        key_words[35] <= 32'b0;
        key_words[36] <= 32'b0;
        key_words[37] <= 32'b0;
        key_words[38] <= 32'b0;
        key_words[39] <= 32'b0;
        key_words[40] <= 32'b0;
        key_words[41] <= 32'b0;
        key_words[42] <= 32'b0;
        key_words[43] <= 32'b0;
        key_words[44] <= 32'b0;
        key_words[45] <= 32'b0;
        key_words[46] <= 32'b0;
        key_words[47] <= 32'b0;
        key_words[48] <= 32'b0;
        key_words[49] <= 32'b0;
        key_words[50] <= 32'b0;
        key_words[51] <= 32'b0;
        key_words[52] <= 32'b0;
        key_words[53] <= 32'b0;
        key_words[54] <= 32'b0;
        key_words[55] <= 32'b0;
        key_words[56] <= 32'b0;
        key_words[57] <= 32'b0;
        key_words[58] <= 32'b0;
        key_words[59] <= 32'b0;
       
    end
    else if(state_reg == KEY_INIT) begin
        //Split input key into key word table
        key_words[0] <= key[255:224];
        key_words[1] <= key[223:192];
        key_words[2] <= key[191:160];
        key_words[3] <= key[159:128];
        key_words[4] <= key[127:96];
        key_words[5] <= key[95:64];
        key_words[6] <= key[63:32];
        key_words[7] <= key[31:0];
    end
    else if(state_reg == KEY_WORDS) begin
        case(kw_state)
            KW_IDLE: begin
                table_idx <= 6'h8;
                rcon_idx  <= 4'b0;
                kw_state  <= 5'b0;
                rk_idx    <= 4'b0;
                kw_state  <= KW_CALC;
                end
            KW_CALC: begin
                    if(table_idx % 8 == 0) begin
                        //Every 8th word uses the G function
                        //Round constants initialized in the initial block
                        //with the frist ten members of GF(2^8)
                        key_words[table_idx] <= g(key_words[table_idx-1], r_con[rcon_idx]) ^ key_words[table_idx-8];
                        rcon_idx <= rcon_idx + 1'b1;
                    end
                    else if(table_idx %4 == 0) begin
                        //Every-other 4th word uses the H function
                        key_words[table_idx] <= h(key_words[table_idx-1]) ^ key_words[table_idx-8];
                    end
                    else begin
                        //Otherwise use a simple XOR
                        key_words[table_idx] <= key_words[table_idx-1] ^ key_words[table_idx-8];
                    end
                    
                    //Increment the table index
                    table_idx <= table_idx + 1'b1;
                    if(table_idx == 60) begin
                        //Key word calculation is done
                        kw_state  <= KW_SCHED;
                        //Reset the table index
                        table_idx <= 6'b0;
                        rk_idx <= 4'b0;
                    end
                end
            KW_SCHED: begin
                    //Every four words forms a subkey
                    if(table_idx != 0 && (table_idx % 4) == 0) begin
                        round_keys[rk_idx][127:96] <= key_words[table_idx-4];
                        round_keys[rk_idx][95:64]  <= key_words[table_idx-3];
                        round_keys[rk_idx][63:32]  <= key_words[table_idx-2];
                        round_keys[rk_idx][31:0]   <= key_words[table_idx-1];
                        //Increment the round key index
                        rk_idx <= rk_idx + 1;
                    end
                    //Increment the table index
                    table_idx <= table_idx + 1;
                    if(table_idx == 61) begin
                        kw_state <= KW_DONE;
                    end
                end
            KW_DONE: begin
                    //Done signal set
                end
        endcase
    end
    else begin
        //Reset when not the active state
        table_idx <= 6'h8;
        rcon_idx  <= 4'b0;
        kw_state  <= 5'b0;
        rk_idx <= 4'b0;
    end
end

///This block initializes the state
///matrix with the input data
always @(posedge clock) begin
    if(resetn == 1'b0) begin
        initial_matrix[3][3] <= 8'b0;
        initial_matrix[2][3] <= 8'b0;
        initial_matrix[1][3] <= 8'b0;
        initial_matrix[0][3] <= 8'b0;
        initial_matrix[3][2] <= 8'b0;
        initial_matrix[2][2] <= 8'b0;
        initial_matrix[1][2] <= 8'b0;
        initial_matrix[0][2] <= 8'b0;
        initial_matrix[3][1] <= 8'b0;
        initial_matrix[2][1] <= 8'b0;
        initial_matrix[1][1] <= 8'b0;
        initial_matrix[0][1] <= 8'b0;
        initial_matrix[3][0] <= 8'b0;
        initial_matrix[2][0] <= 8'b0;
        initial_matrix[1][0] <= 8'b0;
        initial_matrix[0][0] <= 8'b0;
    end
    else if(state_reg == INIT_MATRIX) begin
        //Split the input into the initial matrix
        initial_matrix[3][3] <= data_in[7:0];
        initial_matrix[2][3] <= data_in[15:8];
        initial_matrix[1][3] <= data_in[23:16];
        initial_matrix[0][3] <= data_in[31:24];
        initial_matrix[3][2] <= data_in[39:32];
        initial_matrix[2][2] <= data_in[47:40];
        initial_matrix[1][2] <= data_in[55:48];
        initial_matrix[0][2] <= data_in[63:56];
        initial_matrix[3][1] <= data_in[71:64];
        initial_matrix[2][1] <= data_in[79:72];
        initial_matrix[1][1] <= data_in[87:80];
        initial_matrix[0][1] <= data_in[95:88];
        initial_matrix[3][0] <= data_in[103:96];
        initial_matrix[2][0] <= data_in[111:104];
        initial_matrix[1][0] <= data_in[119:112];
        initial_matrix[0][0] <= data_in[127:120];
    end
end

///This block adds a round key to the state matrix
always @(posedge clock)
begin
    if(resetn == 1'b0) begin
        //Clear the result matrix
        round_key_matrix[3][3] <= 8'b0;
        round_key_matrix[2][3] <= 8'b0;
        round_key_matrix[1][3] <= 8'b0;
        round_key_matrix[0][3] <= 8'b0;
        round_key_matrix[3][2] <= 8'b0;
        round_key_matrix[2][2] <= 8'b0;
        round_key_matrix[1][2] <= 8'b0;
        round_key_matrix[0][2] <= 8'b0;
        round_key_matrix[3][1] <= 8'b0;
        round_key_matrix[2][1] <= 8'b0;
        round_key_matrix[1][1] <= 8'b0;
        round_key_matrix[0][1] <= 8'b0;
        round_key_matrix[3][0] <= 8'b0;
        round_key_matrix[2][0] <= 8'b0;
        round_key_matrix[1][0] <= 8'b0;
        round_key_matrix[0][0] <= 8'b0;
    end
    else if(state_reg == ADD_R0_KEY) begin
        //Apply the round key to the initial matrix
        round_key_matrix[3][3] <= initial_matrix[3][3] ^ round_keys[round_counter][7:0];
        round_key_matrix[2][3] <= initial_matrix[2][3] ^ round_keys[round_counter][15:8];
        round_key_matrix[1][3] <= initial_matrix[1][3] ^ round_keys[round_counter][23:16];
        round_key_matrix[0][3] <= initial_matrix[0][3] ^ round_keys[round_counter][31:24];
        round_key_matrix[3][2] <= initial_matrix[3][2] ^ round_keys[round_counter][39:32];
        round_key_matrix[2][2] <= initial_matrix[2][2] ^ round_keys[round_counter][47:40];
        round_key_matrix[1][2] <= initial_matrix[1][2] ^ round_keys[round_counter][55:48];
        round_key_matrix[0][2] <= initial_matrix[0][2] ^ round_keys[round_counter][63:56];
        round_key_matrix[3][1] <= initial_matrix[3][1] ^ round_keys[round_counter][71:64];
        round_key_matrix[2][1] <= initial_matrix[2][1] ^ round_keys[round_counter][79:72];
        round_key_matrix[1][1] <= initial_matrix[1][1] ^ round_keys[round_counter][87:80];
        round_key_matrix[0][1] <= initial_matrix[0][1] ^ round_keys[round_counter][95:88];
        round_key_matrix[3][0] <= initial_matrix[3][0] ^ round_keys[round_counter][103:96];
        round_key_matrix[2][0] <= initial_matrix[2][0] ^ round_keys[round_counter][111:104];
        round_key_matrix[1][0] <= initial_matrix[1][0] ^ round_keys[round_counter][119:112];
        round_key_matrix[0][0] <= initial_matrix[0][0] ^ round_keys[round_counter][127:120];
    end
    else if(state_reg == ADD_KEY && round_counter < 14) begin
        //Apply the round key to the round key matrix
        round_key_matrix[3][3] <= mix_col_matrix[3][3] ^ round_keys[round_counter][7:0];
        round_key_matrix[2][3] <= mix_col_matrix[2][3] ^ round_keys[round_counter][15:8];
        round_key_matrix[1][3] <= mix_col_matrix[1][3] ^ round_keys[round_counter][23:16];
        round_key_matrix[0][3] <= mix_col_matrix[0][3] ^ round_keys[round_counter][31:24];
        round_key_matrix[3][2] <= mix_col_matrix[3][2] ^ round_keys[round_counter][39:32];
        round_key_matrix[2][2] <= mix_col_matrix[2][2] ^ round_keys[round_counter][47:40];
        round_key_matrix[1][2] <= mix_col_matrix[1][2] ^ round_keys[round_counter][55:48];
        round_key_matrix[0][2] <= mix_col_matrix[0][2] ^ round_keys[round_counter][63:56];
        round_key_matrix[3][1] <= mix_col_matrix[3][1] ^ round_keys[round_counter][71:64];
        round_key_matrix[2][1] <= mix_col_matrix[2][1] ^ round_keys[round_counter][79:72];
        round_key_matrix[1][1] <= mix_col_matrix[1][1] ^ round_keys[round_counter][87:80];
        round_key_matrix[0][1] <= mix_col_matrix[0][1] ^ round_keys[round_counter][95:88];
        round_key_matrix[3][0] <= mix_col_matrix[3][0] ^ round_keys[round_counter][103:96];
        round_key_matrix[2][0] <= mix_col_matrix[2][0] ^ round_keys[round_counter][111:104];
        round_key_matrix[1][0] <= mix_col_matrix[1][0] ^ round_keys[round_counter][119:112];
        round_key_matrix[0][0] <= mix_col_matrix[0][0] ^ round_keys[round_counter][127:120];
    end
    else if(state_reg == ADD_KEY && round_counter == 14) begin
        //Apply the round key to the round key matrix
        round_key_matrix[3][3] <= shift_row_matrix[3][3] ^ round_keys[round_counter][7:0];
        round_key_matrix[2][3] <= shift_row_matrix[2][3] ^ round_keys[round_counter][15:8];
        round_key_matrix[1][3] <= shift_row_matrix[1][3] ^ round_keys[round_counter][23:16];
        round_key_matrix[0][3] <= shift_row_matrix[0][3] ^ round_keys[round_counter][31:24];
        round_key_matrix[3][2] <= shift_row_matrix[3][2] ^ round_keys[round_counter][39:32];
        round_key_matrix[2][2] <= shift_row_matrix[2][2] ^ round_keys[round_counter][47:40];
        round_key_matrix[1][2] <= shift_row_matrix[1][2] ^ round_keys[round_counter][55:48];
        round_key_matrix[0][2] <= shift_row_matrix[0][2] ^ round_keys[round_counter][63:56];
        round_key_matrix[3][1] <= shift_row_matrix[3][1] ^ round_keys[round_counter][71:64];
        round_key_matrix[2][1] <= shift_row_matrix[2][1] ^ round_keys[round_counter][79:72];
        round_key_matrix[1][1] <= shift_row_matrix[1][1] ^ round_keys[round_counter][87:80];
        round_key_matrix[0][1] <= shift_row_matrix[0][1] ^ round_keys[round_counter][95:88];
        round_key_matrix[3][0] <= shift_row_matrix[3][0] ^ round_keys[round_counter][103:96];
        round_key_matrix[2][0] <= shift_row_matrix[2][0] ^ round_keys[round_counter][111:104];
        round_key_matrix[1][0] <= shift_row_matrix[1][0] ^ round_keys[round_counter][119:112];
        round_key_matrix[0][0] <= shift_row_matrix[0][0] ^ round_keys[round_counter][127:120];
    end
end

///This block pefroms S-box substitutions on the round key matrix
///and stores the result in the substitute bytes matrix
always @(posedge clock)
begin
    if(resetn == 1'b0) begin
        //Clear the result matrix
        sub_byte_matrix[3][3] <= 8'b0;
        sub_byte_matrix[2][3] <= 8'b0;
        sub_byte_matrix[1][3] <= 8'b0;
        sub_byte_matrix[0][3] <= 8'b0;
        sub_byte_matrix[3][2] <= 8'b0;
        sub_byte_matrix[2][2] <= 8'b0;
        sub_byte_matrix[1][2] <= 8'b0;
        sub_byte_matrix[0][2] <= 8'b0;
        sub_byte_matrix[3][1] <= 8'b0;
        sub_byte_matrix[2][1] <= 8'b0;
        sub_byte_matrix[1][1] <= 8'b0;
        sub_byte_matrix[0][1] <= 8'b0;
        sub_byte_matrix[3][0] <= 8'b0;
        sub_byte_matrix[2][0] <= 8'b0;
        sub_byte_matrix[1][0] <= 8'b0;
        sub_byte_matrix[0][0] <= 8'b0;
    end
    else if(state_reg == SUB_BYTE) begin
        //Peform S-box Substitutions
        sub_byte_matrix[3][3] <= sbox(round_key_matrix[3][3]);
        sub_byte_matrix[2][3] <= sbox(round_key_matrix[2][3]);
        sub_byte_matrix[1][3] <= sbox(round_key_matrix[1][3]);
        sub_byte_matrix[0][3] <= sbox(round_key_matrix[0][3]);
        sub_byte_matrix[3][2] <= sbox(round_key_matrix[3][2]);
        sub_byte_matrix[2][2] <= sbox(round_key_matrix[2][2]);
        sub_byte_matrix[1][2] <= sbox(round_key_matrix[1][2]);
        sub_byte_matrix[0][2] <= sbox(round_key_matrix[0][2]);
        sub_byte_matrix[3][1] <= sbox(round_key_matrix[3][1]);
        sub_byte_matrix[2][1] <= sbox(round_key_matrix[2][1]);
        sub_byte_matrix[1][1] <= sbox(round_key_matrix[1][1]);
        sub_byte_matrix[0][1] <= sbox(round_key_matrix[0][1]);
        sub_byte_matrix[3][0] <= sbox(round_key_matrix[3][0]);
        sub_byte_matrix[2][0] <= sbox(round_key_matrix[2][0]);
        sub_byte_matrix[1][0] <= sbox(round_key_matrix[1][0]);
        sub_byte_matrix[0][0] <= sbox(round_key_matrix[0][0]);
    end
end

///This block pefroms the Shift Rows transformation of the matrix
always @(posedge clock)
begin
    if(resetn == 1'b0) begin
        //Clear the result matrix
        shift_row_matrix[0][0] <= 8'b0;
        shift_row_matrix[0][1] <= 8'b0;
        shift_row_matrix[0][2] <= 8'b0;
        shift_row_matrix[0][3] <= 8'b0;
        shift_row_matrix[1][0] <= 8'b0;
        shift_row_matrix[1][1] <= 8'b0;
        shift_row_matrix[1][2] <= 8'b0;
        shift_row_matrix[1][3] <= 8'b0;
        shift_row_matrix[2][0] <= 8'b0;
        shift_row_matrix[2][1] <= 8'b0;
        shift_row_matrix[2][2] <= 8'b0;
        shift_row_matrix[2][3] <= 8'b0;
        shift_row_matrix[3][0] <= 8'b0;
        shift_row_matrix[3][1] <= 8'b0;
        shift_row_matrix[3][2] <= 8'b0;
        shift_row_matrix[3][3] <= 8'b0;
    end
    else if(state_reg == SHIFT_ROW) begin
        //No Shift
        shift_row_matrix[0][0] <= sub_byte_matrix[0][0];
        shift_row_matrix[0][1] <= sub_byte_matrix[0][1];
        shift_row_matrix[0][2] <= sub_byte_matrix[0][2];
        shift_row_matrix[0][3] <= sub_byte_matrix[0][3];

        //Shift once
        shift_row_matrix[1][0] <= sub_byte_matrix[1][1];
        shift_row_matrix[1][1] <= sub_byte_matrix[1][2];
        shift_row_matrix[1][2] <= sub_byte_matrix[1][3];
        shift_row_matrix[1][3] <= sub_byte_matrix[1][0];

        //Shift twice
        shift_row_matrix[2][0] <= sub_byte_matrix[2][2];
        shift_row_matrix[2][1] <= sub_byte_matrix[2][3];
        shift_row_matrix[2][2] <= sub_byte_matrix[2][0];
        shift_row_matrix[2][3] <= sub_byte_matrix[2][1];
        //Shift three places
        shift_row_matrix[3][0] <= sub_byte_matrix[3][3];
        shift_row_matrix[3][1] <= sub_byte_matrix[3][0];
        shift_row_matrix[3][2] <= sub_byte_matrix[3][1];
        shift_row_matrix[3][3] <= sub_byte_matrix[3][2];
    end
end

///This block pefroms the Mix Columns transformation of the matrix
always @(posedge clock) begin
    if(resetn == 1'b0) begin
        //Clear the result matrix
        mix_col_matrix[0][0] <= 8'b0;
        mix_col_matrix[0][1] <= 8'b0;
        mix_col_matrix[0][2] <= 8'b0;
        mix_col_matrix[0][3] <= 8'b0;
        mix_col_matrix[1][0] <= 8'b0;
        mix_col_matrix[1][1] <= 8'b0;
        mix_col_matrix[1][2] <= 8'b0;
        mix_col_matrix[1][3] <= 8'b0;
        mix_col_matrix[2][0] <= 8'b0;
        mix_col_matrix[2][1] <= 8'b0;
        mix_col_matrix[2][2] <= 8'b0;
        mix_col_matrix[2][3] <= 8'b0;
        mix_col_matrix[3][0] <= 8'b0;
        mix_col_matrix[3][1] <= 8'b0;
        mix_col_matrix[3][2] <= 8'b0;
        mix_col_matrix[3][3] <= 8'b0;
    end
    else if(state_reg == MIX_COL) begin
        //Perform the column mixing
        mix_col_matrix[0][0] <= gf2mult(2, shift_row_matrix[0][0]) ^ gf2mult(3, shift_row_matrix[1][0]) ^ shift_row_matrix[2][0] ^ shift_row_matrix[3][0];
        mix_col_matrix[1][0] <= shift_row_matrix[0][0] ^ gf2mult(2, shift_row_matrix[1][0]) ^ gf2mult(3, shift_row_matrix[2][0]) ^ shift_row_matrix[3][0];
        mix_col_matrix[2][0] <= shift_row_matrix[0][0] ^ shift_row_matrix[1][0] ^ gf2mult(2, shift_row_matrix[2][0]) ^ gf2mult(3, shift_row_matrix[3][0]);            
        mix_col_matrix[3][0] <= gf2mult(3, shift_row_matrix[0][0]) ^ shift_row_matrix[1][0] ^ shift_row_matrix[2][0] ^ gf2mult(2, shift_row_matrix[3][0]);

        mix_col_matrix[0][1] <= gf2mult(2, shift_row_matrix[0][1]) ^ gf2mult(3, shift_row_matrix[1][1]) ^ shift_row_matrix[2][1] ^ shift_row_matrix[3][1];
        mix_col_matrix[1][1] <= shift_row_matrix[0][1] ^ gf2mult(2, shift_row_matrix[1][1]) ^ gf2mult(3, shift_row_matrix[2][1]) ^ shift_row_matrix[3][1];
        mix_col_matrix[2][1] <= shift_row_matrix[0][1] ^ shift_row_matrix[1][1] ^ gf2mult(2, shift_row_matrix[2][1]) ^ gf2mult(3, shift_row_matrix[3][1]);            
        mix_col_matrix[3][1] <= gf2mult(3, shift_row_matrix[0][1]) ^ shift_row_matrix[1][1] ^ shift_row_matrix[2][1] ^ gf2mult(2, shift_row_matrix[3][1]);

        mix_col_matrix[0][2] <= gf2mult(2, shift_row_matrix[0][2]) ^ gf2mult(3, shift_row_matrix[1][2]) ^ shift_row_matrix[2][2] ^ shift_row_matrix[3][2];
        mix_col_matrix[1][2] <= shift_row_matrix[0][2] ^ gf2mult(2, shift_row_matrix[1][2]) ^ gf2mult(3, shift_row_matrix[2][2]) ^ shift_row_matrix[3][2];
        mix_col_matrix[2][2] <= shift_row_matrix[0][2] ^ shift_row_matrix[1][2] ^ gf2mult(2, shift_row_matrix[2][2]) ^ gf2mult(3, shift_row_matrix[3][2]);            
        mix_col_matrix[3][2] <= gf2mult(3, shift_row_matrix[0][2]) ^ shift_row_matrix[1][2] ^ shift_row_matrix[2][2] ^ gf2mult(2, shift_row_matrix[3][2]);

        mix_col_matrix[0][3] <= gf2mult(2, shift_row_matrix[0][3]) ^ gf2mult(3, shift_row_matrix[1][3]) ^ shift_row_matrix[2][3] ^ shift_row_matrix[3][3];
        mix_col_matrix[1][3] <= shift_row_matrix[0][3] ^ gf2mult(2, shift_row_matrix[1][3]) ^ gf2mult(3, shift_row_matrix[2][3]) ^ shift_row_matrix[3][3];
        mix_col_matrix[2][3] <= shift_row_matrix[0][3] ^ shift_row_matrix[1][3] ^ gf2mult(2, shift_row_matrix[2][3]) ^ gf2mult(3, shift_row_matrix[3][3]);            
        mix_col_matrix[3][3] <= gf2mult(3, shift_row_matrix[0][3]) ^ shift_row_matrix[1][3] ^ shift_row_matrix[2][3] ^ gf2mult(2, shift_row_matrix[3][3]);
    end
end

///This block populates the output data from the final matrix
always @(posedge clock)
begin
    if(resetn == 1'b0) begin
        //Clear the output
        data_out <= 128'b0;
    end
    else if(state_reg == OUTPUT) begin
        //Populate the output register
        data_out[7:0] <=     round_key_matrix[3][3];
        data_out[15:8] <=    round_key_matrix[2][3];
        data_out[23:16] <=   round_key_matrix[1][3];
        data_out[31:24] <=   round_key_matrix[0][3];
        data_out[39:32] <=   round_key_matrix[3][2];
        data_out[47:40] <=   round_key_matrix[2][2];
        data_out[55:48] <=   round_key_matrix[1][2];
        data_out[63:56] <=   round_key_matrix[0][2];
        data_out[71:64] <=   round_key_matrix[3][1];
        data_out[79:72] <=   round_key_matrix[2][1];
        data_out[87:80] <=   round_key_matrix[1][1];
        data_out[95:88] <=   round_key_matrix[0][1];
        data_out[103:96] <=  round_key_matrix[3][0];
        data_out[111:104] <= round_key_matrix[2][0];
        data_out[119:112] <= round_key_matrix[1][0];
        data_out[127:120] <= round_key_matrix[0][0];
       
    end
end

///This always block is the main state machine
///for the encryption process.
always @(posedge clock) begin
    ///Reset logic
    if(resetn == 1'b0) begin
        state_reg <= IDLE;
    end
    else begin
    ///State machine
        case(state_reg)
            IDLE: begin //Start the operation
                    if( enable_pulse == 1'b1)
                    begin
                        done <= 1'b0;
                        state_reg <= KEY_INIT;
                    end
                end
            KEY_INIT: begin //Initialize key words
                state_reg <= KEY_WORDS;
                end
            KEY_WORDS: begin //Calcualte round key words
                if(KEY_WORDS_DONE == 1'b1)
                    state_reg <= INIT_MATRIX;
                end
            INIT_MATRIX: begin //Initial state matrix
                    state_reg <= ADD_R0_KEY;
                    round_counter <= 6'b0; //Reset the round counter
                end
            ADD_R0_KEY: begin //Add the first round key
                   round_counter <= round_counter + 1;
                   state_reg <= SUB_BYTE;
            end
            SUB_BYTE: begin //Perform the byte substitution layer
                state_reg <= SHIFT_ROW;
            end
            SHIFT_ROW: begin //Perform the shift row layer
                if(round_counter < 14) begin
                    state_reg <= MIX_COL;
                end
                else begin
                    //Shift rows is skipped in the final round
                    state_reg <= ADD_KEY;
                end
            end
            MIX_COL: begin // Perform Mix Columns transformation
                state_reg <= ADD_KEY;
            end
            ADD_KEY: begin
                if(round_counter < 14) begin
                    //Repeat the layers
                    state_reg <= SUB_BYTE;
                end
                else begin
                    //Finalize in the output state
                    state_reg <= OUTPUT;
                end
                round_counter <= round_counter + 1'b1;
            end
            OUTPUT: begin
                state_reg <= DONE;
            end
            DONE: begin
                done <= 1'b1;
                state_reg <= IDLE;
            end
            default:
                state_reg <= IDLE;
        endcase
    end
end

endmodule