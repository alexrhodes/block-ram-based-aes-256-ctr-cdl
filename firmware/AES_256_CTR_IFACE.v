/*
Licensed under the MIT License.

Copyright (c) 2021 - present Alex Rhodes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
`timescale 1ns / 1ps

module AES_256_CTR_IFACE
    (
        input clock,                   //Clock signal
        input enable,                  //Enable signal
        input resetn,                  //Reset signal
        input [255:0] key,             //Input key
        input [127:0] iv,              //Initialization vector
        input [31:0]  base_addr,       //Base address of the data
        input [31:0]  num_blocks,      //Number of AES blocks
        output reg [31:0] bram_addr,   //BRAM Address
        output reg [127:0] bram_din,   //BRAM data in
        input  [127:0] bram_dout,      //BRAM data out
        output reg bram_en,            //BRAM enable
        output reg bram_rst,           //BRAM reset
        output reg [31:0] bram_wen,    //BRAM write enable
        output wire done               //Done signal
    );

parameter [7:0] IDLE          = 7'd1,   //State machine idle
                RWORD         = 7'd2,   //Read a 128-bit block
                WAITRWORD     = 7'd3,   //Wait for the read
                CTR_START     = 7'd4,   //Start the CTR module
                WAIT_CTR_DONE = 7'd5,   //Wait for the CTR module to finish
                CTR_STOP      = 7'd6,   //Stop the CTR module
                PREPARE_WWORD = 7'd7,   //Set up for writing the block back to memory
                WWORD         = 7'd8,   //Start the write back
                WAITWWORD     = 7'd9,   //Wait for the write back to finish
                ADV_ADDR      = 7'd10,  //Advance the address to the next block
                DONE          = 7'd11;  //Operation done

//Registers for the AES CTR module
reg  ctr_resetn;
reg  ctr_enable;
reg  [127:0] ctr_data_in;
wire [127:0] ctr_data_out;
reg  [127:0] ctr_iv;
reg  [255:0] ctr_key;
wire ctr_valid;



// This register holds the current state
reg [7:0] state_reg;

//This register holds the 32-bit words remaining
reg [31:0] blocks_remaining;

//This register holds the current address
reg [31:0] current_addr;

//Stall counter for BRAM read/write
reg [3:0] stall_counter;

//"Done" signal
reg done_reg;
assign done = (done_reg == 1'b1);

//This block generates an enable pulse
//to start the state machine when the
//enable signal is set high
//Assign enable high for one clock cycle
reg enable_ff1; //Enable pulse flipflops
reg enable_ff2;
assign enable_pulse    = (!enable_ff2) && enable_ff1;

always @(posedge clock) begin                                                        
    if (resetn == 1'b0 || enable == 1'b0)                                                   
        begin
        ///Reset                                                                 
            enable_ff1 <= 1'b0;                                                   
            enable_ff2 <= 1'b0;                                                   
        end                                                                               
    else                                                                       
        begin 
        /// Enable pulse
            enable_ff1 <= enable;
            enable_ff2 <= enable_ff1;                                                        
        end                                                                      
end

//Initialization block
initial begin
    //State reg
    state_reg <= IDLE;
    //Counter module registers
    ctr_resetn = 1'b0;
    ctr_enable = 1'b0;
    ctr_data_in = 127'b0;

    //BRAM stall counter
    stall_counter = 4'b0;
    
    //Enable flip flops
    enable_ff1 = 1'b0;
    enable_ff2 = 1'b0;
end

///This always block is the main state machine
///for the encryption process.
always @(posedge clock) begin
    ///Reset logic
    if(resetn == 1'b0) begin
        state_reg <= IDLE;

        //Reset modules
        ctr_resetn <= 1'b0;
        bram_rst <= 1'b1;

        //Counter module registers
        ctr_enable <= 1'b0;
        ctr_data_in <= 127'b0;

        //BRAM control registes
        bram_addr <=  32'b0;
        bram_din  <=  128'b0;
        bram_en   <=  1'b0;
        bram_wen  <=  32'b0;

    end
    else begin
    ///State machine
        case(state_reg)
            IDLE: begin
                if( enable_pulse == 1'b1)
                begin
                    //On the enable pulse, reset the AES core
                    ctr_resetn <= 1'b1;

                    //Reset the BRAM control
                    bram_rst <= 1'b0;
                    
                    //Clear the done signal
                    done_reg <= 1'b0;
                    
                    //Store the base address
                    current_addr <= base_addr;
                    
                    //Store the number of words
                    blocks_remaining <= num_blocks;
                    
                    //Set the key
                    ctr_key <= key;
                    
                    //Set the IV
                    ctr_iv <= iv;

                    //Reset the stall ctr
                    stall_counter <= 4'b0;

                    //Enable the BRAM
                    bram_en <= 1'b1;
                    
                    //Move to set words
                    state_reg <= RWORD;
                end
            end
            RWORD: begin
                //Set the read address
                bram_addr <= current_addr;

                //Move to wait word 1
                state_reg <= WAITRWORD;
            end
            WAITRWORD: begin
                if(stall_counter < 4'h3) begin
                   stall_counter <= stall_counter + 1; 
                end
                else begin
                    //Store the read word into the CTR input
                    ctr_data_in <= bram_dout;

                    //Reset the stall counter
                    stall_counter <= 4'b0;

                    //Decrement the words remaining
                    blocks_remaining <= blocks_remaining - 1;

                    //Move to the next word
                    state_reg <= CTR_START;
                end
            end
            CTR_START: begin
                
                //Enable the counter module
                ctr_enable <= 1'b1;
                
                //Stall before checking done 
                if(stall_counter < 4'h3) begin
                   stall_counter <= stall_counter + 1; 
                end
                else
                begin
                    //Reset the stall counter
                    stall_counter <= 0;

                    //Move to the wait done state
                    state_reg <= WAIT_CTR_DONE;

                end
            end
            WAIT_CTR_DONE: begin
                if(ctr_valid == 1'b1) begin
                    state_reg <= CTR_STOP;
                end
            end
            CTR_STOP: begin
                //Disable the counter
                ctr_enable <= 1'b0;
                state_reg <= PREPARE_WWORD;
            end
            PREPARE_WWORD: begin
                //Set the block ram address
                bram_addr <= current_addr;

                //Set the input data
                bram_din <= ctr_data_out;

                state_reg <= WWORD;
            end
            WWORD: begin
                //Set the write enable
                bram_wen <= 32'hFFFFFFFF;

                //Wait for the word
                state_reg <= WAITWWORD;
            end
            WAITWWORD: begin
                //Stall before checking done 
                if(stall_counter < 4'h3) begin
                   stall_counter <= stall_counter + 1; 
                end
                else begin
                    //Clear the write enable
                    bram_wen <= 32'b0;
                    
                    state_reg <= ADV_ADDR;
                end
            end
            ADV_ADDR: begin
                    //Reset the stall counter
                    stall_counter <= 0;

                    //Increment the address
                    current_addr <= current_addr + 16;
                    
                    //Check if there are more words to encrypt
                    if(blocks_remaining == 31'b0) begin
                        state_reg <= DONE;
                    end
                    else begin
                        state_reg <= RWORD;
                    end
            end
            DONE: begin
                

                //Reset the AES core
                ctr_resetn <= 1'b0;

                //Reset the BRAM control
                bram_rst <= 1'b1;
                
                //Set the done signal
                done_reg <= 1'b1;
                //Return to the IDLE state
                state_reg <= IDLE;
            end
            default:
                state_reg <= IDLE;
        endcase
    end
end



//AES CTR module
AES_256_CTR aes_ctr
    (
        .clock(clock),         
        .enable(ctr_enable),        
        .resetn(ctr_resetn),
        .key(ctr_key),           
        .iv(ctr_iv),            
        .data_in(ctr_data_in),
        .data_out(ctr_data_out),
        .valid(ctr_valid)
    );

endmodule

