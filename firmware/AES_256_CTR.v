/*
Licensed under the MIT License.

Copyright (c) 2021 - present Alex Rhodes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
`timescale 1ns / 1ps

module AES_256_CTR
    (
        input clock,                 //Clock signal
        input enable,                //Enable signal
        input resetn,                //Reset signal
        input [255:0] key,           //Input key
        input [127:0] iv,            //Initialization vector
        input [127:0] data_in,       //Input data
        output reg [127:0] data_out, //Output data
        output wire valid            //All blocks complete signal
    );

parameter [4:0] IDLE              =   4'b0000, //State machine is idle
                INIT              =   4'b0001, //Initialization after reset
                WAIT_BLOCK        =   4'b0010, //Wait for the next block
                START_ENCRYPT     =   4'b0011, //Start encryption
                WAIT_START        =   4'b0100, //Wait for AES start ffs
                WAIT_START2       =   4'b0101, 
                WAIT_START3       =   4'b0110,
                WAIT_ENCRYPT      =   4'b0111, //Wait for the AES core to finish
                DISABLE_AES       =   4'b1000, //Stop the AES core
                XOR_DATA          =   4'b1001, //XOR the input data
                INC_IV            =   4'b1010, //Increment the IV
                DONE              =   4'b1011; //Operation done

// AES core enable signal
reg aes_enable;

// AES core reset signal
reg aes_resetn;

//AES core IV
reg [127:0] aes_iv;

// AES core output cipher stream
wire [127:0] aes_cipher_stream;

// AES core valid signal
wire aes_valid;

// This register holds the current state
reg [3:0] state_reg;     

//"Done" signal used to set the valid output
reg done;
assign valid = (done == 1'b1);

//Initialization block
initial begin
    done = 1'b0;
    enable_ff1 = 1'b0;
    enable_ff2 = 1'b0;
    aes_enable = 1'b0;
end


//This block generates an enable pulse
//to start the state machine when the
//enable signal is set high
//Assign enable high for one clock cycle
reg enable_ff1; //Enable pulse flipflops
reg enable_ff2;
assign enable_pulse    = (!enable_ff2) && enable_ff1;

always @(posedge clock) begin                                                        
    if (resetn == 1'b0 || enable == 1'b0)                                                   
        begin
        ///Reset                                                                 
            enable_ff1 <= 1'b0;                                                   
            enable_ff2 <= 1'b0;                                                   
        end                                                                               
    else                                                                       
        begin 
        /// Enable pulse
            enable_ff1 <= enable;
            enable_ff2 <= enable_ff1;                                                        
        end                                                                      
    end

///This always block is the main state machine
///for the encryption process.
always @(posedge clock) begin
    ///Reset logic
    if(resetn == 1'b0) begin
        state_reg <= IDLE;
        aes_resetn <= 1'b0;
        
    end
    else begin
    ///State machine
        case(state_reg)
            IDLE: begin
                if( enable_pulse == 1'b1)
                begin
                    //On the enable pulse, reset the AES core
                    aes_resetn <= 1'b1;
                    //Clear the done signal
                    done <= 1'b0;
                    //Move to initialization
                    state_reg <= INIT;
                end
            end
            INIT: begin
                //Copy the IV into the AES IV input
                aes_iv <= iv;
                //Move to starting the encryption
                state_reg <= START_ENCRYPT;
            end
            //If the module hasn't been reset,
            //continue the operation as a single 
            //encryption/decryption
            WAIT_BLOCK: begin
                if( enable_pulse == 1'b1)
                begin
                    //When enabled, clear the done flag
                    done <= 1'b0;
                    //Move to starting the encryption
                    state_reg <= START_ENCRYPT;
                end
            end
            START_ENCRYPT: begin //Encrypt the IV
                //Enable the AES core
                aes_enable <= 1'b1;
                //Move to stall cycles
                state_reg <= WAIT_START;
            end
            WAIT_START: begin //Stall for AES to start
                state_reg <= WAIT_START2;
            end
            WAIT_START2: begin
                state_reg <= WAIT_START3;
            end
            WAIT_START3: begin
                state_reg <= WAIT_ENCRYPT;
            end
            WAIT_ENCRYPT: begin
                //Wait for the AES core to finish
                if(aes_valid == 1'b1) begin
                    //When AES is done, move to disable AES
                    state_reg <= DISABLE_AES;
                end
            end
            DISABLE_AES: begin
                //Disable the AES block
                aes_enable <= 1'b0;
                //Move to XOR of the input data
                state_reg <= XOR_DATA;
            end
            XOR_DATA: begin 
                // XOR the input data into the output
                data_out <= data_in ^ aes_cipher_stream;
                
                // Move to increment the IV
                state_reg <= INC_IV;
            end
            INC_IV: begin 
                
                //Increment the IV
                aes_iv <= aes_iv + 1'b1;
                //Move to done
                state_reg <= DONE;
            end
            DONE: begin
                //Set the done signal
                done <= 1'b1;
                //Wait for the next block
                state_reg <= WAIT_BLOCK;
            end
            default:
                state_reg <= IDLE;
        endcase
    end
end


//AES core 
AES256 aes (
    .clock(clock),
    .enable(aes_enable),
    .resetn(aes_resetn),
    .key(key),
    .data_in(aes_iv),
    .data_out(aes_cipher_stream),
    .valid(aes_valid)
);

endmodule

