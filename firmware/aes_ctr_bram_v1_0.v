/*
Licensed under the MIT License.

Copyright (c) 2021 - present Alex Rhodes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Modified from auto-generated file
Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.

*/
`timescale 1 ns / 1 ps

    module aes_ctr_bram_v1_0 #
    (
        // Users to add parameters here

        // User parameters ends
        // Do not modify the parameters beyond this line


        // Parameters of Axi Slave Bus Interface S00_AXI_CTRL
        parameter integer C_S00_AXI_CTRL_DATA_WIDTH    = 32,
        parameter integer C_S00_AXI_CTRL_ADDR_WIDTH    = 6
    )
    (
        // Users to add ports here
        output wire [31:0] bram_addr,   //BRAM Address
        output wire [127:0] bram_din,   //BRAM data in
        input  wire [127:0] bram_dout,  //BRAM data out
        output wire bram_en,            //BRAM enable
        output wire bram_rst,           //BRAM reset
        output wire [31:0] bram_wen,    //BRAM write enable
        // User ports ends
        // Do not modify the ports beyond this line


        // Ports of Axi Slave Bus Interface S00_AXI_CTRL
        input wire  s00_axi_ctrl_aclk,
        input wire  s00_axi_ctrl_aresetn,
        input wire [C_S00_AXI_CTRL_ADDR_WIDTH-1 : 0] s00_axi_ctrl_awaddr,
        input wire [2 : 0] s00_axi_ctrl_awprot,
        input wire  s00_axi_ctrl_awvalid,
        output wire  s00_axi_ctrl_awready,
        input wire [C_S00_AXI_CTRL_DATA_WIDTH-1 : 0] s00_axi_ctrl_wdata,
        input wire [(C_S00_AXI_CTRL_DATA_WIDTH/8)-1 : 0] s00_axi_ctrl_wstrb,
        input wire  s00_axi_ctrl_wvalid,
        output wire  s00_axi_ctrl_wready,
        output wire [1 : 0] s00_axi_ctrl_bresp,
        output wire  s00_axi_ctrl_bvalid,
        input wire  s00_axi_ctrl_bready,
        input wire [C_S00_AXI_CTRL_ADDR_WIDTH-1 : 0] s00_axi_ctrl_araddr,
        input wire [2 : 0] s00_axi_ctrl_arprot,
        input wire  s00_axi_ctrl_arvalid,
        output wire  s00_axi_ctrl_arready,
        output wire [C_S00_AXI_CTRL_DATA_WIDTH-1 : 0] s00_axi_ctrl_rdata,
        output wire [1 : 0] s00_axi_ctrl_rresp,
        output wire  s00_axi_ctrl_rvalid,
        input wire  s00_axi_ctrl_rready
    );
// Instantiation of Axi Bus Interface S00_AXI_CTRL
    aes_ctr_bram_v1_0_S00_AXI_CTRL # ( 
        .C_S_AXI_DATA_WIDTH(C_S00_AXI_CTRL_DATA_WIDTH),
        .C_S_AXI_ADDR_WIDTH(C_S00_AXI_CTRL_ADDR_WIDTH)
    ) aes_ctr_bram_v1_0_S00_AXI_CTRL_inst (
        .S_AXI_ACLK(s00_axi_ctrl_aclk),
        .S_AXI_ARESETN(s00_axi_ctrl_aresetn),
        .S_AXI_AWADDR(s00_axi_ctrl_awaddr),
        .S_AXI_AWPROT(s00_axi_ctrl_awprot),
        .S_AXI_AWVALID(s00_axi_ctrl_awvalid),
        .S_AXI_AWREADY(s00_axi_ctrl_awready),
        .S_AXI_WDATA(s00_axi_ctrl_wdata),
        .S_AXI_WSTRB(s00_axi_ctrl_wstrb),
        .S_AXI_WVALID(s00_axi_ctrl_wvalid),
        .S_AXI_WREADY(s00_axi_ctrl_wready),
        .S_AXI_BRESP(s00_axi_ctrl_bresp),
        .S_AXI_BVALID(s00_axi_ctrl_bvalid),
        .S_AXI_BREADY(s00_axi_ctrl_bready),
        .S_AXI_ARADDR(s00_axi_ctrl_araddr),
        .S_AXI_ARPROT(s00_axi_ctrl_arprot),
        .S_AXI_ARVALID(s00_axi_ctrl_arvalid),
        .S_AXI_ARREADY(s00_axi_ctrl_arready),
        .S_AXI_RDATA(s00_axi_ctrl_rdata),
        .S_AXI_RRESP(s00_axi_ctrl_rresp),
        .S_AXI_RVALID(s00_axi_ctrl_rvalid),
        .S_AXI_RREADY(s00_axi_ctrl_rready),
        .bram_addr(bram_addr),
        .bram_din(bram_din),
        .bram_dout(bram_dout),
        .bram_en(bram_en),
        .bram_rst(bram_rst),
        .bram_wen(bram_wen)
    );

    // Add user logic here

    // User logic ends

    endmodule
